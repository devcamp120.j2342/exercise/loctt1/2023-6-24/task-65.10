package com.devcamp.countryregionapi.reponsitorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryregionapi.models.Region;

public interface RegionRepository extends JpaRepository<Region, Long> {

}
